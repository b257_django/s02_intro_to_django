from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import ToDoItem
# Create your views here.
# importing HttpResponse from django and using it within the  index function to return a response once that function is called

def index(request): # 'request' is a built-in keyword from django that gives us access to the request object which will contain data from the user like input, tokens, etc.
    # return HttpResponse('Hello fromo views.py file!'),
    todoitem_list = ToDoItem.objects.all()
    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])
    template = loader.get_template('todolist/index.html')
    context = {
        'todoitem_list' : todoitem_list
    }
    return HttpResponse(template.render(context, request))

def todoitem(request, todoitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response % todoitem_id)